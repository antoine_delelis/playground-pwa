import firebase from 'firebase'

var config = {
  apiKey: 'AIzaSyA_12uD9OP2rjWnYOdMevxdCDkj9KYNxf8',
  authDomain: 'pandora-b6eac.firebaseapp.com',
  databaseURL: 'https://pandora-b6eac.firebaseio.com',
  projectId: 'pandora-b6eac',
  storageBucket: 'pandora-b6eac.appspot.com',
  messagingSenderId: '995793093657'
}
firebase.initializeApp(config)

export default {
  database: firebase.database()
}
